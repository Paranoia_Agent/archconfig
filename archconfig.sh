#!/bin/bash

### Updating System
sudo pacman -Syu --noconfirm

echo -e "\nFinished System Update\n"

### YAY Install
cd
mkdir -p Documents/git
cd Documents/git

git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si --noconfirm

echo -e "\nFinished Paru Install\n"

### Pacman Packages
sudo pacman -S bspwm sxhkd sddm rofi dunst playerctl unclutter firefox qutebrowser python-adblock rbw kitty thunar thunar-archive-plugin thunar-volman gvfs gvfs-mtp udevil ranger w3m ffmpegthumbnailer tumbler flameshot starship python-pywal mpv viewnior zathura zathura-pdf-poppler pipewire pipewire-alsa pipewire-pulse pipewire-jack pavucontrol nm-connection-editor polkit-gnome lxappearance-gtk3 qt5ct mousepad neofetch neovim btop wget git xdg-user-dirs xf86-input-wacom noto-fonts noto-fonts-cjk krita gimp inkscape --noconfirm

echo -e "\nFinished Installing Pacman Packages\n"

### AUR Packages
paru -S picom-jonaburg-fix polybar zscroll-git betterlockscreen xidlehook bibata-cursor-theme nerd-fonts-jetbrains-mono nerd-fonts-fantasque-sans-mono --noconfirm

echo -e "\nFinished Installing AUR Packages\n"

cd
sudo chmod +x .config/polybar/scripts/player.sh
sudo chmod +x .config/polybar/scripts/polywins.sh
sudo chmod +x .config/xidlehook/lockscreen
sudo chmod +x .config/ranger/scope.sh
sudo chmod +x .config/bspwm/bspwmrc

sudo systemctl enable sddm

#localectl --no-convert set-x11-keymap br pc104 abnt2 caps:swapescape
#localectl --no-convert set-x11-keymap us pc86 intl caps:swapescape

echo -e "\nFinished Setup\n"
